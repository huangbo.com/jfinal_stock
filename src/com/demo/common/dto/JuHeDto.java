package com.demo.common.dto;

public class JuHeDto {
	private int error_code;
	private String reason;
	private JuHeResultDto result;

	public int getError_code() {
		return error_code;
	}

	public void setError_code(int error_code) {
		this.error_code = error_code;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public JuHeResultDto getResult() {
		return result;
	}

	public void setResult(JuHeResultDto result) {
		this.result = result;
	}

}
