package com.demo.common.dto;

import java.math.BigDecimal;

public class JuHeResultDataDto {
	private String symbol;/*代码*/
	private String name;/*名称*/
	private BigDecimal trade;/*最新价*/
	private BigDecimal pricechange;/*涨跌额*/
	private BigDecimal changepercent;/*涨跌幅*/
	private BigDecimal buy;/*买入*/
	private BigDecimal sell;/*卖出*/
	private BigDecimal settlement;/*昨收*/
	private BigDecimal open;/*今开*/
	private BigDecimal high;/*最高*/
	private BigDecimal low;/*最低*/
	private BigDecimal volume;/*成交量*/
	private BigDecimal amount;/*成交额*/
	private String code;/*股票代码*/
	private String ticktime;/*时间*/

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getTrade() {
		return trade;
	}

	public void setTrade(BigDecimal trade) {
		this.trade = trade;
	}

	public BigDecimal getPricechange() {
		return pricechange;
	}

	public void setPricechange(BigDecimal pricechange) {
		this.pricechange = pricechange;
	}

	public BigDecimal getChangepercent() {
		return changepercent;
	}

	public void setChangepercent(BigDecimal changepercent) {
		this.changepercent = changepercent;
	}

	public BigDecimal getBuy() {
		return buy;
	}

	public void setBuy(BigDecimal buy) {
		this.buy = buy;
	}

	public BigDecimal getSell() {
		return sell;
	}

	public void setSell(BigDecimal sell) {
		this.sell = sell;
	}

	public BigDecimal getSettlement() {
		return settlement;
	}

	public void setSettlement(BigDecimal settlement) {
		this.settlement = settlement;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTicktime() {
		return ticktime;
	}

	public void setTicktime(String ticktime) {
		this.ticktime = ticktime;
	}

}
