package com.demo.common.dto;

public class RetData {
	private Stockinfo[] stockinfo;

	public Stockinfo[] getStockinfo() {
		return stockinfo;
	}

	public void setStockinfo(Stockinfo[] stockinfo) {
		this.stockinfo = stockinfo;
	}

}
