package com.demo.common.dto;

import java.math.BigDecimal;
import java.util.Date;

public class Stockinfo {
	private String name;
	private String code;
	private Date date;
	private String time;
	private BigDecimal OpenningPrice;
	private BigDecimal closingPrice;
	private BigDecimal currentPrice;
	private BigDecimal hPrice;
	private BigDecimal lPrice;
	private BigDecimal competitivePrice;
	private BigDecimal auctionPrice;
	private BigDecimal totalNumber;
	private BigDecimal turnover;
	private BigDecimal increase;
	private BigDecimal buyOne;
	private BigDecimal buyOnePrice;
	private BigDecimal buyTwo;
	private BigDecimal buyTwoPrice;
	private BigDecimal buyThree;
	private BigDecimal buyThreePrice;
	private BigDecimal buyFour;
	private BigDecimal buyFourPrice;
	private BigDecimal buyFive;
	private BigDecimal buyFivePrice;
	private BigDecimal sellOne;
	private BigDecimal sellOnePrice;
	private BigDecimal sellTwo;
	private BigDecimal sellTwoPrice;
	private BigDecimal sellThree;
	private BigDecimal sellThreePrice;
	private BigDecimal sellFour;
	private BigDecimal sellFourPrice;
	private BigDecimal sellFive;
	private BigDecimal sellFivePrice;
	private String dayurl;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public BigDecimal getOpenningPrice() {
		return OpenningPrice;
	}

	public void setOpenningPrice(BigDecimal openningPrice) {
		OpenningPrice = openningPrice;
	}

	public BigDecimal getClosingPrice() {
		return closingPrice;
	}

	public void setClosingPrice(BigDecimal closingPrice) {
		this.closingPrice = closingPrice;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	public BigDecimal gethPrice() {
		return hPrice;
	}

	public void sethPrice(BigDecimal hPrice) {
		this.hPrice = hPrice;
	}

	public BigDecimal getlPrice() {
		return lPrice;
	}

	public void setlPrice(BigDecimal lPrice) {
		this.lPrice = lPrice;
	}

	public BigDecimal getCompetitivePrice() {
		return competitivePrice;
	}

	public void setCompetitivePrice(BigDecimal competitivePrice) {
		this.competitivePrice = competitivePrice;
	}

	public BigDecimal getAuctionPrice() {
		return auctionPrice;
	}

	public void setAuctionPrice(BigDecimal auctionPrice) {
		this.auctionPrice = auctionPrice;
	}

	public BigDecimal getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(BigDecimal totalNumber) {
		this.totalNumber = totalNumber;
	}

	public BigDecimal getTurnover() {
		return turnover;
	}

	public void setTurnover(BigDecimal turnover) {
		this.turnover = turnover;
	}

	public BigDecimal getIncrease() {
		return increase;
	}

	public void setIncrease(BigDecimal increase) {
		this.increase = increase;
	}

	public BigDecimal getBuyOne() {
		return buyOne;
	}

	public void setBuyOne(BigDecimal buyOne) {
		this.buyOne = buyOne;
	}

	public BigDecimal getBuyOnePrice() {
		return buyOnePrice;
	}

	public void setBuyOnePrice(BigDecimal buyOnePrice) {
		this.buyOnePrice = buyOnePrice;
	}

	public BigDecimal getBuyTwo() {
		return buyTwo;
	}

	public void setBuyTwo(BigDecimal buyTwo) {
		this.buyTwo = buyTwo;
	}

	public BigDecimal getBuyTwoPrice() {
		return buyTwoPrice;
	}

	public void setBuyTwoPrice(BigDecimal buyTwoPrice) {
		this.buyTwoPrice = buyTwoPrice;
	}

	public BigDecimal getBuyThree() {
		return buyThree;
	}

	public void setBuyThree(BigDecimal buyThree) {
		this.buyThree = buyThree;
	}

	public BigDecimal getBuyThreePrice() {
		return buyThreePrice;
	}

	public void setBuyThreePrice(BigDecimal buyThreePrice) {
		this.buyThreePrice = buyThreePrice;
	}

	public BigDecimal getBuyFour() {
		return buyFour;
	}

	public void setBuyFour(BigDecimal buyFour) {
		this.buyFour = buyFour;
	}

	public BigDecimal getBuyFourPrice() {
		return buyFourPrice;
	}

	public void setBuyFourPrice(BigDecimal buyFourPrice) {
		this.buyFourPrice = buyFourPrice;
	}

	public BigDecimal getBuyFive() {
		return buyFive;
	}

	public void setBuyFive(BigDecimal buyFive) {
		this.buyFive = buyFive;
	}

	public BigDecimal getBuyFivePrice() {
		return buyFivePrice;
	}

	public void setBuyFivePrice(BigDecimal buyFivePrice) {
		this.buyFivePrice = buyFivePrice;
	}

	public BigDecimal getSellOne() {
		return sellOne;
	}

	public void setSellOne(BigDecimal sellOne) {
		this.sellOne = sellOne;
	}

	public BigDecimal getSellOnePrice() {
		return sellOnePrice;
	}

	public void setSellOnePrice(BigDecimal sellOnePrice) {
		this.sellOnePrice = sellOnePrice;
	}

	public BigDecimal getSellTwo() {
		return sellTwo;
	}

	public void setSellTwo(BigDecimal sellTwo) {
		this.sellTwo = sellTwo;
	}

	public BigDecimal getSellTwoPrice() {
		return sellTwoPrice;
	}

	public void setSellTwoPrice(BigDecimal sellTwoPrice) {
		this.sellTwoPrice = sellTwoPrice;
	}

	public BigDecimal getSellThree() {
		return sellThree;
	}

	public void setSellThree(BigDecimal sellThree) {
		this.sellThree = sellThree;
	}

	public BigDecimal getSellThreePrice() {
		return sellThreePrice;
	}

	public void setSellThreePrice(BigDecimal sellThreePrice) {
		this.sellThreePrice = sellThreePrice;
	}

	public BigDecimal getSellFour() {
		return sellFour;
	}

	public void setSellFour(BigDecimal sellFour) {
		this.sellFour = sellFour;
	}

	public BigDecimal getSellFourPrice() {
		return sellFourPrice;
	}

	public void setSellFourPrice(BigDecimal sellFourPrice) {
		this.sellFourPrice = sellFourPrice;
	}

	public BigDecimal getSellFive() {
		return sellFive;
	}

	public void setSellFive(BigDecimal sellFive) {
		this.sellFive = sellFive;
	}

	public BigDecimal getSellFivePrice() {
		return sellFivePrice;
	}

	public void setSellFivePrice(BigDecimal sellFivePrice) {
		this.sellFivePrice = sellFivePrice;
	}

	public String getDayurl() {
		return dayurl;
	}

	public void setDayurl(String dayurl) {
		this.dayurl = dayurl;
	}

}
