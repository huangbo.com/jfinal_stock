package com.demo.common.dto;

public class JuHeResultDto {
	private int totalCount;/*总条数*/
	private int page;/*当前页*/
	private int num;/*显示条数*/
	private JuHeResultDataDto[] data;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public JuHeResultDataDto[] getData() {
		return data;
	}

	public void setData(JuHeResultDataDto[] data) {
		this.data = data;
	}

}
