package com.demo.quartzJob;

import java.util.HashMap;
import java.util.Map;

import org.quartz.SchedulerException;

import com.common.dto.ReturnDto;
import com.common.plugin.QuartzPlugin;
import com.demo.common.model.QuartzJob;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;

public class QuartzJobController extends Controller {
	
	private static final Log log = Log.getLog(QuartzJobController.class);
	
	public void index() {
		setAttr("quartzPage", QuartzJob.dao.paginate(getParaToInt(0, 1), 10));
		render("list.html");
	}

	public void paginate() {
		int page = getParaToInt("page");
		int rows = getParaToInt("rows");
		Page<QuartzJob> paginate = QuartzJob.dao.paginate(page, rows);
		Map result = new HashMap();
		result.put("total", paginate.getTotalRow());
		result.put("rows", paginate.getList());
		renderJson(result);
	}
	
	public void add() {
	}

	public void save() {
		//保存数据
		QuartzJob quartzJob = getModel(QuartzJob.class);
		//启动定时任务
		try {
			ReturnDto dto = new ReturnDto();
			QuartzPlugin.manager.initJob(quartzJob, Class.forName(quartzJob.getJobClassName()));
			if(quartzJob.save()) {
				dto.setSuccess(true);
				dto.setMsg("新增成功!");
			} else {
				dto.setSuccess(false);
				dto.setMsg("新增失败!");
			}
			renderJson(dto);
		} catch (ClassNotFoundException e) {
			log.error("启动定时任务失败!" + quartzJob.getJobClassName() + "不存在:" + e.getMessage(), e);
			e.printStackTrace();
			renderError(403);
		} catch (SchedulerException e) {
			log.error("启动定时任务失败!" + e.getMessage(), e);
			e.printStackTrace();
		}
	}

	public void edit() {
		renderJson(QuartzJob.dao.findById(getParaToInt()));
	}

	public void update() {
		QuartzJob quartzJob = getModel(QuartzJob.class);
		quartzJob.setJobStatus(QuartzPlugin.STATUS_PAUSE);
		try {
			ReturnDto dto = new ReturnDto();
			//先删除，后新增，再暂停
			QuartzPlugin.manager.deleteJob(quartzJob);
			QuartzPlugin.manager.initJob(quartzJob, Class.forName(quartzJob.getJobClassName()));
			QuartzPlugin.manager.pauseJob(quartzJob);
			//QuartzPlugin.manager.pauseJob(quartzJob);
			if(quartzJob.update()) {
				dto.setSuccess(true);
				dto.setMsg("修改成功!");
			} else {
				dto.setSuccess(false);
				dto.setErrMsg("修改失败!");
			}
			renderJson(dto);
		} catch (SchedulerException e) {
			e.printStackTrace();
			renderError(403);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			renderError(403);
		}
		
	}
	
	/**
	 * 逻辑删除
	 * */
	public void delete() {
		QuartzJob quartzJob = QuartzJob.dao.findById(getParaToInt());
		quartzJob.setJobStatus(QuartzPlugin.STATUS_DEL);
		try {
			ReturnDto dto = new ReturnDto();
			QuartzPlugin.manager.deleteJob(quartzJob);
			if(quartzJob.update()) {
				dto.setSuccess(true);
				dto.setMsg("删除成功!");
			} else {
				dto.setSuccess(false);
				dto.setErrMsg("删除失败!");
			}
			renderJson(dto);
		} catch (SchedulerException e) {
			log.error("删除定时任务发生异常:" + e.getMessage(), e);
			renderError(403);
		}
	}
	
	/**
	 * 恢复定时任务
	 * */
	public void resume() {
		QuartzJob quartzJob = QuartzJob.dao.findById(getParaToInt());
		ReturnDto dto = new ReturnDto();
		try {
			QuartzPlugin.manager.resumeJob(quartzJob);
			quartzJob.setJobStatus(QuartzPlugin.STATUS_RESUME);
			if(quartzJob.update()) {
				dto.setSuccess(true);
				dto.setMsg("恢复任务成功!");
			} else {
				dto.setSuccess(false);
				dto.setErrMsg("恢复任务失败!");
			}
		} catch (Exception e) {
			log.error("恢复定时任务发生异常:" + e.getMessage(), e);
			dto.setSuccess(false);
			dto.setErrMsg("恢复任务失败:" + e.getMessage());
		} finally {
			renderJson(dto);
		}
	}
	
	/**
	 * 暂停定时任务
	 * */
	public void pause() {
		QuartzJob quartzJob = QuartzJob.dao.findById(getParaToInt());
		ReturnDto dto = new ReturnDto();
		try {
			QuartzPlugin.manager.pauseJob(quartzJob);
			quartzJob.setJobStatus(QuartzPlugin.STATUS_PAUSE);
			if(quartzJob.update()) {
				dto.setSuccess(true);
				dto.setMsg("暂停任务成功!");
			} else {
				dto.setSuccess(false);
				dto.setErrMsg("暂停任务失败!");
			}
		} catch (Exception e) {
			log.error("暂停定时任务发生异常:" + e.getMessage(), e);
			dto.setSuccess(false);
			dto.setErrMsg("恢复任务失败:" + e.getMessage());
		} finally {
			renderJson(dto);
		}
		
	}
	
	/**
	 * 立即运行一次
	 * */
	public void runOnce() {
		QuartzJob quartzJob = QuartzJob.dao.findById(getParaToInt());
		ReturnDto dto = new ReturnDto();
		try {
			QuartzPlugin.manager.runJob(quartzJob);
			dto.setSuccess(true);
			dto.setMsg("任务运行成功!");
		} catch (Exception e) {
			log.error("暂停定时任务发生异常:" + e.getMessage(), e);
			dto.setSuccess(false);
			dto.setErrMsg("恢复任务失败:" + e.getMessage());
		} finally {
			renderJson(dto);
		}
		
	}
}
