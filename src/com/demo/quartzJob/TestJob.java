package com.demo.quartzJob;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class TestJob implements Job {

	@Override
	public void execute(JobExecutionContext job) throws JobExecutionException {
		System.out.println("我是测试任务！" + job.getJobDetail().getKey());
	}

}
