package com.demo.recommend;

import com.common.interceotpr.AuthInterceptor;
import com.common.interceotpr.ExceptionInterceptor;
import com.common.util.FormatUtil;
import com.demo.common.model.Recommend;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.ActiveRecordException;

public class RecommendController extends Controller {

	@Before({RecommendValidator.class, AuthInterceptor.class, ExceptionInterceptor.class})
	public void save() throws Exception{
		Recommend recommend = getModel(Recommend.class);
		recommend.setUserId(AuthInterceptor.getUserId(getRequest()));
		recommend.setCreateTime(FormatUtil.formatDate(FormatUtil.getDate(), FormatUtil.pattern_ymd_hms));
		try {
			if(!recommend.save())
				throw new IllegalArgumentException("保存失败!");
		} catch (ActiveRecordException e) {
			if(e.getCause().toString().indexOf("com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException") != -1) {
				throw new IllegalArgumentException("该信号已经添加推荐!");
			}
		}; 
	}
}
