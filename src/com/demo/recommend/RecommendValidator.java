package com.demo.recommend;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class RecommendValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateDouble("recommend.buyPrice", "errMsg", "推荐价格只能为数字!");
	}

	@Override
	protected void handleError(Controller c) {
		c.setAttr("success", false);
		c.renderJson();
	}

}
