package com.demo.user;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class LoginValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRegex("user.username", "^[a-zA-Z0-9_]{5,12}$", "loginmsg_username", "用户名不能输入特殊字符!");
		validateRequired("user.username", "loginmsg_username", "请输入用户名!");
		
		validateString("user.password", 5, 18, "loginmsg_password", "请输入正确的密码!");
	}

	@Override
	protected void handleError(Controller c) {
		c.setAttr("success", false);
		c.renderJson();
	}

}
