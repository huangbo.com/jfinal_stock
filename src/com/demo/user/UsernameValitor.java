package com.demo.user;

import java.util.List;

import com.demo.common.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class UsernameValitor extends Validator {

	@Override
	protected void validate(Controller controller) {
		validateRequired("user.password", "regmsg_password", "请输入密码!");
		validateString("user.password", 6, 18, "regmsg_password", "密码长度为6-18位!");
		validateEqualField("user.password", "reg.password2", "regmsg_password", "两次输入的密码不一致!");

		//检查用户名是否重复
		List<User> userList = User.dao.findByUsername(controller.getPara("user.username"));
		if(userList.size() > 0) {
			addError("regmsg_username", "该用户名已存在!");
		}

		validateRequired("user.username", "regmsg_username", "请输入用户名!");
		validateString("user.username", 2, 12, "regmsg_username", "用户名长度为5-12个字符!");
		validateRegex("user.username", "^[a-zA-Z0-9_]{5,12}$", "regmsg_username", "用户名不能输入特殊字符!");
	}

	@Override
	protected void handleError(Controller controller) {
		controller.setAttr("success", false);
		controller.renderJson();
	}
}
