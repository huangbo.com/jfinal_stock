package com.demo.stock;

import com.demo.common.model.Stock;
import com.jfinal.core.Controller;

public class StockController extends Controller {

	public void index() {
		setAttr("stockPage", Stock.dao.paginate(getParaToInt(0, 1), 10));
		render("list.html");
	}
}
