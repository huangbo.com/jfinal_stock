package com.demo.stocksignal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alibaba.druid.util.StringUtils;
import com.common.dto.ReturnDto;
import com.common.util.Conditions;
import com.demo.common.model.Stocksignal;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;

public class StocksignalController extends Controller {

	public void index() {
		render("list.html");
	}
	
	public void paginate() {
		int page = getParaToInt("page");
		int rows = getParaToInt("rows");
		
		Conditions condi = new Conditions();
		
		String stockCode = getPara("stockCode");
		if(StrKit.notBlank(stockCode)) {
			condi.setValueQuery(Conditions.FUZZY_RIGHT, "stockCode", stockCode);
		}
		
		String signalType = getPara("signalType");
		if(StrKit.notBlank(signalType)) {
			condi.setValueQuery(Conditions.EQUAL, "signalType", signalType);
		}
		
		Page<Stocksignal> paginate = Stocksignal.dao.paginate(page, rows, condi);
		Map result = new HashMap();
		result.put("total", paginate.getTotalRow());
		result.put("rows", paginate.getList());
		renderJson(result);
	}
	
	public void paginateToJson() {
		int pageNumber = getParaToInt("pageNumber", 1);
		int pageSize = getParaToInt("pageSize",20);
		
		int signalType = getParaToInt("signalType");
		Conditions condi = new Conditions();
		condi.setValueQuery(Conditions.EQUAL, "signalType", signalType);
		
		renderJson(Stocksignal.dao.paginate(pageNumber, pageSize, condi));
	}
	
	/**
	 * 删除方法
	 * */
	@Before(Tx.class)
	public void delete() {
		String stockCodesStr = getPara("stockCodesStr");
		if(!StringUtils.isEmpty(stockCodesStr)) {
			String[] stockCodes = stockCodesStr.split(",");
			List<Stocksignal> signalList = Stocksignal.dao.findListByStockCodes(stockCodes);
			for (Stocksignal stocksignal : signalList) {
				stocksignal.delete();
			}
		}
		ReturnDto rtDto = new ReturnDto();
		rtDto.setSuccess(true);
		renderJson(rtDto);
	}
}
