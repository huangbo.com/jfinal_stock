package com.quartz.timertask;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.common.util.FormatUtil;
import com.jfinal.log.Log;  
  
/** 
 * 任务调度工厂类 
 * @author Joyce.Luo 
 * @date 2015-3-31 下午03:38:35 
 * @version V3.0 
 * @since Tomcat6.0,Jdk1.6 
 * @copyright Copyright (c) 2015 
 */  
public class QuartzJobFactory implements Job {  
    private static final Log logger = Log.getLog(QuartzJobFactory.class);  
      
    @Override  
    public void execute(JobExecutionContext context) throws JobExecutionException {  
        /*QuartzJob scheduleJob = (QuartzJob)context.getMergedJobDataMap().get("backup_job");  
        logger.info("定时任务开始执行，任务名称[" + scheduleJob.getJobName() + "]");  
        Date previousFireTime = context.getPreviousFireTime();  
        if(null != previousFireTime){  
            logger.info("定时任务上次调度时间：" + FormatUtil.formatDate(previousFireTime));  
        }  
        logger.info("定时任务下次调度时间：" + FormatUtil.formatDate(context.getNextFireTime()));  */
        // 执行业务逻辑  
        System.out.println("定时任务执行！");
    }  
}  