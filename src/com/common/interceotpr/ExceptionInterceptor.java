package com.common.interceotpr;

import com.common.dto.ReturnDto;
import com.common.util.ConstantErrCode;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class ExceptionInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		ReturnDto rtDto = new ReturnDto();
		try {
			inv.invoke();
			rtDto.setSuccess(true);
			rtDto.setErrCode(ConstantErrCode.NORMAL);
		} catch (Exception e) {
			rtDto.setErrCode(ConstantErrCode.FAIL);
			rtDto.setSuccess(false);
			rtDto.setErrMsg(e.getMessage());
		} finally {
			controller.renderJson(rtDto);
		}
	}

}
