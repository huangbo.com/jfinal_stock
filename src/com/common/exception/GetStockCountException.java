package com.common.exception;

public class GetStockCountException extends RuntimeException {
	public GetStockCountException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 407166788613642657L;

}
