package com.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FormatUtil {

	private static String DEFAULT_PATTERN = "yyyy-MM:dd HH:mm:ss";
	
	public static final String pattern_ym = "yyyy-MM"; // pattern_ym
	public static final String pattern_ymd = "yyyy-MM-dd"; // pattern_ymd
	public static final String pattern_ymd_hm = "yyyy-MM-dd HH:mm"; // pattern_ymd hm
	public static final String pattern_ymd_hms = "yyyy-MM-dd HH:mm:ss"; // pattern_ymd time
	public static final String pattern_ymd_hms_s = "yyyy-MM-dd HH:mm:ss:SSS"; // pattern_ymd timeMillisecond
	
	public static String formatDate(Date date) {
		return formatDate(date, DEFAULT_PATTERN);
	}
	
	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public static Date getDate() {
		return new Date();
	}
	
	public static long getDateTime() {
		return new Date().getTime();
	}
	
	/**
	 * 返回两个日期之间隔了多少天
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDateDaySpace(Date start, Date end) {
		int day = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000));
		return day;
	}
	
	/**
	 * 返回两个日期之间隔了多少分钟
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDateMinuteSpace(Date start, Date end) {
		int hour = (int) ((end.getTime() - start.getTime()) / (60 * 1000));
		return hour;
	}
	
	/**
	 * 返回两个日期之间隔了多少天，包含开始、结束时间
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<String> getDaySpaceDate(Date start, Date end) {
		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.setTime(start);
		fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
		fromCalendar.set(Calendar.MINUTE, 0);
		fromCalendar.set(Calendar.SECOND, 0);
		fromCalendar.set(Calendar.MILLISECOND, 0);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(end);
		toCalendar.set(Calendar.HOUR_OF_DAY, 0);
		toCalendar.set(Calendar.MINUTE, 0);
		toCalendar.set(Calendar.SECOND, 0);
		toCalendar.set(Calendar.MILLISECOND, 0);

		List<String> dateList = new LinkedList<String>();

		long dayCount = (toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24);
		if (dayCount < 0) {
			return dateList;
		}

		dateList.add(formatDate(fromCalendar.getTime(), pattern_ymd));

		for (int i = 0; i < dayCount; i++) {
			fromCalendar.add(Calendar.DATE, 1);// 增加一天
			dateList.add(formatDate(fromCalendar.getTime(), pattern_ymd));
		}

		return dateList;
	}
}
