package com.common.util;

public interface ConstantErrCode {
	
	public static final Integer NORMAL = 0;
	
	public static final Integer NO_LOGIN = 1;
	
	public static final Integer FAIL = 2;
}
