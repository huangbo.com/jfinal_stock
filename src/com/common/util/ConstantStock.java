package com.common.util;

/**
 * 股票常量表
 * @author jiang
 *
 */
public class ConstantStock {

	public static final String  MIN_AMPLITUDE = "stock.min_amplitude";//获取股票的最小整幅
	
	public static final String MAX_WAVA = "stock.max_wava";//最大波动
}
