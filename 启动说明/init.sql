/*
Navicat MySQL Data Transfer

Source Server         : loclhost
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : jfinal_quartz

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-08-17 17:42:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `blog`
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('3', 'test 2', 'test 2');
INSERT INTO `blog` VALUES ('4', 'test 3', 'test 3');
INSERT INTO `blog` VALUES ('5', 'test 5', 'test 5');

-- ----------------------------
-- Table structure for `quartz_job`
-- ----------------------------
DROP TABLE IF EXISTS `quartz_job`;
CREATE TABLE `quartz_job` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 主键',
  `job_name` varchar(100) NOT NULL COMMENT '任务名称',
  `job_group` varchar(100) NOT NULL DEFAULT '''default_group''' COMMENT '任务分组',
  `job_class_name` varchar(500) NOT NULL COMMENT '对应的任务名称',
  `cron_expression` varchar(100) NOT NULL COMMENT 'cron表达式',
  `job_status` smallint(6) NOT NULL DEFAULT '0' COMMENT '任务状态',
  `job_info` varchar(1000) DEFAULT NULL COMMENT '任务描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `任务ID` (`job_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quartz_job
-- ----------------------------
INSERT INTO `quartz_job` VALUES ('1', 'test', 'test_group', 'com.job.TestJob', '0 0 0 0 0 0 ?', '2', '这是一个测试定时任务');
INSERT INTO `quartz_job` VALUES ('2', 'test1', 'test_group', 'com.demo.quartzJob.TestJob', '0/3 * * * * ? ', '2', '这是一个测试任务');

CREATE TABLE `stock` (
`id`  bigint NOT NULL ,
`stockCode`  int NOT NULL COMMENT '股票代码' ,
`stockName`  varchar(20) NOT NULL COMMENT '股票名称' ,
`stockUrl`  varchar(100) NOT NULL COMMENT '股票url' ,
`tradingSignals`  smallint NOT NULL DEFAULT 0 COMMENT '是否有交易信号：0（无）；1（有）' ,
`leftEye`  smallint NULL DEFAULT 0 COMMENT '是否有左眼：0（无）；1（有）' ,
`rightEye`  smallint NULL DEFAULT 0 COMMENT '是否有右眼：0（无）；1（有）' ,
`signalsTime`  datetime COMMENT '信号发生时间' ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `stock`
MODIFY COLUMN `id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST ,
ADD UNIQUE INDEX (`stockCode`) ;

ALTER TABLE `stock`
ADD COLUMN `stockType`  smallint NOT NULL DEFAULT 0 COMMENT '0为上证，1为深证' AFTER `id`;

-- ----------------------------
-- Table structure for `stockinfo`
-- ----------------------------
DROP TABLE IF EXISTS `stockinfo`;
CREATE TABLE `stockinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '昨日收盘价',
  `stockId` bigint(20) DEFAULT NULL COMMENT '关联股票ID',
  `date` datetime DEFAULT NULL COMMENT '交易记录时间',
  `time` datetime DEFAULT NULL COMMENT '具体时间',
  `OpenningPrice` decimal(10,2) DEFAULT NULL COMMENT '今日开盘价',
  `closingPrice` decimal(10,2) DEFAULT NULL,
  `currentPrice` decimal(10,2) DEFAULT NULL COMMENT '当前价格',
  `hPrice` decimal(10,2) DEFAULT NULL COMMENT '今日最高价',
  `lPrice` decimal(10,2) DEFAULT NULL COMMENT '今日最低价',
  `competitivePrice` decimal(10,2) DEFAULT NULL COMMENT '买一报价',
  `auctionPrice` decimal(10,2) DEFAULT NULL COMMENT '卖一报价',
  `totalNumber` bigint(20) DEFAULT NULL COMMENT '成交的股票数',
  `turnover` decimal(20,2) DEFAULT NULL COMMENT '成交额，以元为单位',
  `increase` decimal(10,8) DEFAULT NULL COMMENT '涨幅',
  `buyOne` decimal(10,2) DEFAULT NULL COMMENT '买一 ',
  `buyOnePrice` decimal(10,2) DEFAULT NULL COMMENT '买一价格',
  `buyTwo` decimal(10,2) DEFAULT NULL COMMENT '买二',
  `buyTwoPrice` decimal(10,2) DEFAULT NULL COMMENT '买二价格',
  `buyThree` decimal(10,2) DEFAULT NULL COMMENT '买三',
  `buyThreePrice` decimal(10,2) DEFAULT NULL COMMENT '买三价格',
  `buyFour` decimal(10,2) DEFAULT NULL COMMENT '买四',
  `buyFourPrice` decimal(10,2) DEFAULT NULL COMMENT '买四价格',
  `buyFive` decimal(10,2) DEFAULT NULL,
  `buyFivePrice` decimal(10,2) DEFAULT NULL COMMENT '买五价格',
  `sellOne` decimal(10,2) DEFAULT NULL COMMENT '卖一',
  `sellOnePrice` decimal(10,2) DEFAULT NULL COMMENT '卖一价格',
  `sellTwo` decimal(10,2) DEFAULT NULL COMMENT '卖二',
  `sellTwoPrice` decimal(10,2) DEFAULT NULL COMMENT '卖二价格',
  `sellThree` decimal(10,2) DEFAULT NULL COMMENT '卖三',
  `sellThreePrice` decimal(10,2) DEFAULT NULL COMMENT '卖三价格',
  `sellFour` decimal(10,2) DEFAULT NULL COMMENT '卖四',
  `sellFourPrice` decimal(10,2) DEFAULT NULL COMMENT '卖四价格',
  `sellFive` decimal(10,2) DEFAULT NULL COMMENT '卖五',
  `sellFivePrice` decimal(10,2) DEFAULT NULL COMMENT '卖五价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 修改time类型为varchar----------------------------------------
ALTER TABLE `stockinfo`
MODIFY COLUMN `time`  varchar(5) NULL DEFAULT NULL COMMENT '具体时间' AFTER `date`;

-- 修改股票行情和股票表关联的字段
ALTER TABLE `stockinfo`
CHANGE COLUMN `stockId` `stockCode`  int(20) NULL DEFAULT NULL COMMENT '股票code' AFTER `id`;

-- 修改股票代码的数据类型
ALTER TABLE `stock`
DROP COLUMN `stockType`,
MODIFY COLUMN `stockCode`  varchar(10) NOT NULL COMMENT '股票代码' AFTER `id`;

-- 修改股票code为varchar
ALTER TABLE `stockinfo`
MODIFY COLUMN `stockCode`  varchar(20) NULL DEFAULT NULL COMMENT '股票code' AFTER `id`;

-- 添加卖出信号字段
ALTER TABLE `stock`
CHANGE COLUMN `tradingSignals` `buySignals`  smallint(6) NOT NULL DEFAULT 0 COMMENT '买入信号：0（无）；1（有）' AFTER `stockUrl`,
ADD COLUMN `sellSignals`  smallint(6) NULL COMMENT '卖出信号：0（无）；1（有）' AFTER `buySignals`;

-- 
ALTER TABLE `stockinfo`
MODIFY COLUMN `time`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '具体时间' AFTER `date`;

--
ALTER TABLE `stock`
ADD COLUMN `dailyChar`  varchar(100) NULL COMMENT '日线url' AFTER `signalsTime`;

--
ALTER TABLE `stock`
ADD COLUMN `weight`  decimal(10,2) NULL COMMENT '权值' AFTER `dailyChar`;

-- 创建交易信号表
CREATE TABLE `stockSignal` (
`id`  bigint NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`stockCode`  varchar(10) NOT NULL COMMENT '股票编号' ,
`signalType`  smallint NOT NULL DEFAULT 0 COMMENT '0：无效信号;1：买入信号;2：卖出信号' ,
`signalDate`  datetime NULL COMMENT '信号发生时间' ,
`isLeftEye`  smallint NULL COMMENT '是否有左耳.0:无;1:有' ,
`isRightEye`  smallint NULL COMMENT '是否有右耳.0:无;1:有' ,
`weight`  decimal(10,2) NULL COMMENT '信号强度' ,
PRIMARY KEY (`id`)
)
;

--
ALTER TABLE `stockSignal`
ADD COLUMN `dailyChar`  varchar(100) NULL COMMENT '日线url' AFTER `weight`;

-- 创建聚合对应的数据库表
CREATE TABLE `jh_stockinfo` (
`id`  bigint NOT NULL AUTO_INCREMENT ,
`symbol`  varchar(10) NULL COMMENT '代码' ,
`name`  varchar(20) NULL COMMENT '名称' ,
`trade`  decimal(20,3) NULL COMMENT '最新价' ,
`pricechange`  decimal(20,3) NULL COMMENT '涨跌额' ,
`changepercent`  decimal(20,3) NULL COMMENT '涨跌幅' ,
`buy`  decimal(20,3) NULL COMMENT '买入' ,
`sell`  decimal(20,3) NULL COMMENT '卖出' ,
`settlement`  decimal(20,3) NULL COMMENT '昨收' ,
`open`  decimal(20,3) NULL COMMENT '今开' ,
`high`  decimal(20,3) NULL COMMENT '最高' ,
`low`  decimal(20,3) NULL COMMENT '最低' ,
`volume`  decimal(20,3) NULL COMMENT '成交量' ,
`amount`  decimal(20,3) NULL COMMENT '成交额' ,
`ticktime`  varchar(50) NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `jh_stockinfo`
ADD COLUMN `code`  varchar(10) NULL COMMENT '股票代码' AFTER `amount`;

ALTER TABLE `jh_stockinfo`
ADD COLUMN `createtime`  varchar(20) NULL COMMENT '信号时间' AFTER `code`;

ALTER TABLE `stocksignal`
ADD COLUMN `symbol`  varchar(10) NULL AFTER `id`;

ALTER TABLE `stocksignal`
MODIFY COLUMN `signalDate`  varchar(20) NULL DEFAULT NULL COMMENT '信号发生时间' AFTER `signalType`;

ALTER TABLE `stocksignal`
ADD COLUMN `isFight`  smallint NULL DEFAULT 0 COMMENT '0:无;1:有' AFTER `isRightEye`;

-- 2016年9月22日 10:15:05 创建用户表
CREATE TABLE `user` (
`id`  int NOT NULL AUTO_INCREMENT ,
`username`  varchar(128) NOT NULL COMMENT '用户名' ,
`salt`  varchar(256) NOT NULL COMMENT '盐' ,
`password`  varchar(256) NOT NULL COMMENT '密码' ,
`status`  int NULL DEFAULT 0 COMMENT '用户状态 0:正常 1:被锁定 2:失效' ,
`errorcount`  int NULL DEFAULT 0 COMMENT '密码错误次数' ,
`chineseName`  varchar(128) NULL COMMENT '中文名' ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `user`
ADD COLUMN `stopDate`  datetime NULL COMMENT '密码错误次数达到上限后帐号被停用的时间' AFTER `chineseName`;

-- 2016年9月26日 23:33:29 创建推荐表
CREATE TABLE `recommend` (
`id`  int NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`userId`  int NOT NULL COMMENT '用户id' ,
`symbol`  varchar(8) NULL COMMENT '代码' ,
`stockCode`  varchar(6) NULL COMMENT '股票代码' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `唯一索引` (`userId`, `symbol`) USING HASH 
)
;

-- 2016年9月27日 13:45:12 创建关注表
CREATE TABLE `optional` (
`id`  int NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`userId`  int NULL COMMENT '用户ID' ,
`symbol`  varchar(8) NULL COMMENT '股票编号' ,
`stockCode`  varchar(6) NULL COMMENT '股票编码' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `唯一约束` (`userId`, `symbol`) USING HASH 
)
;

ALTER TABLE `recommend`
ADD COLUMN `createTime`  varchar(20) NULL COMMENT '创建时间' AFTER `stockCode`;

ALTER TABLE `optional`
ADD COLUMN `createTime`  varchar(20) NULL COMMENT '创建时间' AFTER `stockCode`;

ALTER TABLE `recommend`
ADD COLUMN `buyPrice`  decimal(10,2) NULL DEFAULT 0 COMMENT '建议买入价格' AFTER `stockCode`;
