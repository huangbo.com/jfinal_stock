function page_ajax(url,page_function_name,operate_msg){
	$.ajax({
		type: "get",
		async:true,
		url: url,
		dataType:"json",
		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
		beforeSend:function(){ 
		    var h=document.body.clientHeight; 
		    $("<div class=\"getdata-mask\"></div>").css({width:"100%",height:'100%'}).appendTo("body"); 
		  },
		 complete:function(data){ 
			   // $('.datagrid-mask-msg').remove(); 
			    $('.getdata-mask').remove(); 
			  },
		success: function (msg) {
			var fun_eval = eval(page_function_name);
			new fun_eval(msg); 
		},
		error:function(xhr,st,err){//执行错误走此方法
			//page_outof_logon(xhr);
			//console.info(operate_msg+curr_msg['failed']+"."+page_get_show_err_mag(xhr.responseText));
			//message.alert('错误', operate_msg+curr_msg['failed']+"."+page_get_show_err_mag(xhr.responseText));
			return false;
		}
	});
}

function page_ajax_no_async(url,page_function_name,operate_msg){
	$.ajax({
		type: "get",
		async:false,
		url: url,
		dataType:"json",
		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
		beforeSend:function(){ 
		    var h=document.body.clientHeight; 
		    $("<div class=\"getdata-mask\"></div>").css({width:"100%",height:'100%'}).appendTo("body"); 
		  },
		 complete:function(data){ 
			   // $('.datagrid-mask-msg').remove(); 
			    $('.getdata-mask').remove(); 
			  },
		success: function (msg) {
			var fun_eval = eval(page_function_name);
			new fun_eval(msg); 
		},
		error:function(xhr,st,err){//执行错误走此方法
			page_outof_logon(xhr);
			console.info(operate_msg+curr_msg['failed']+"."+page_get_show_err_mag(xhr.responseText));
			message.alert('错误', operate_msg+curr_msg['failed']+"."+page_get_show_err_mag(xhr.responseText));
			return false;
		}
	});
}

function page_post_ajax(url,p_data,page_function_name,operate_msg){
	$.ajax({
		type: "post",
		url: url,
		dataType:"json",
		async: true,
		data: p_data,
		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
		success: function (msg) {
			var fun_eval = eval(page_function_name);
			new fun_eval(msg); 
		},
		error:function(xhr,st,err){//执行错误走此方法
			console.log(err);
			return false;
		}
	});
}